package org.acme;

import dtu.rs.entities.PaymentData;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.Transaction;
import manager.SimpleDTUPay;
import manager.AccountsManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/payments")
public class PaymentResource {
    static SimpleDTUPay dtuPay = new SimpleDTUPay();
    AccountsManager accountsManager = new AccountsManager();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response payToMerchant(PaymentData paymentData)
    {
        try{
            Transaction transaction = dtuPay.pay(paymentData);
            //accountsManager.addNewTransaction(transaction);
            return Response.noContent().build();
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @GET
    public String getResponse(){
        return "Success";
    }
}