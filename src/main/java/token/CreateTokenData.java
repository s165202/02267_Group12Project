package token;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class CreateTokenData {
    public String cid;
    public int number;

    @JsonbCreator
    public CreateTokenData(@JsonbProperty("cid") String cid,
                           @JsonbProperty("number") int number){
        this.cid = cid;
        this.number = number;
    }
}