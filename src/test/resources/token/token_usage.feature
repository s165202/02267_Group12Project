Feature: Invalidate tokens

  @CucumberTest.TokenManager.Usage
  Scenario: Customer uses 2 valid tokens
    Given a customer with 2 tokens
    When the customer uses 2 valid tokens
    Then after usage the customer has 0 tokens

  @CucumberTest.TokenManager.Usage
  Scenario: Customer uses 2 invalid tokens
    Given a customer with 2 tokens
    When the customer uses 2 invalid tokens
    Then after usage the customer has 2 tokens


