package rest;

import token.CreateTokenData;
import token.Token;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;


public class RESTTokenManagementClient {

    static String port = "8080/";
    static WebTarget baseUrl = ClientBuilder.newClient().target("http://localhost:" + port);

    public List<Token> getTokensFromCustomerId(String cid) throws Exception{
        Response response = baseUrl.path("tokens").path("customer").path(cid).request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200){
            return response.readEntity(new GenericType<List<Token>>() {});
        } else {
            throw new Exception("Failed to find customer");
        }
    }
    
    public Token getToken(String tid) throws Exception{
        Response response = baseUrl.path("tokens").path(tid).request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 202) {
            return response.readEntity(Token.class);
        } else {
            throw new Exception("Failed to find token");
        }
    }

    public boolean validateToken(String tid){
        return baseUrl.path("tokens").path("valid").path(tid).request().get().getStatus() == 200;
    }

    public List<Token> generateTokensForCustomer(CreateTokenData tokenData) throws Exception{
        
        Response response = baseUrl
                .path("tokens")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.entity(tokenData, MediaType.APPLICATION_JSON));
        if (response.getStatus() == 200){
            return response.readEntity(new GenericType<List<Token>>() {});
        } else {
            throw new Exception("Request failed with status code: " + response.getStatus());
        }
    }

    public void invalidateToken(String tid) throws Exception {
        Response response = baseUrl.path("tokens").path(tid).request().delete();
        if (response.getStatus() != 200){
            throw new Exception("failed to invalidate token, status code: "+ response.getStatus());
        }
    }

    public void clean() throws Exception{
        Response response = baseUrl.path("tokens").request().delete();
        if (response.getStatus() != 200) throw new Exception("Failed to clean");
    }
}


