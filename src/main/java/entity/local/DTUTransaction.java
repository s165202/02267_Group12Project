package entity.local;

import dtu.ws.fastmoney.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement
public class DTUTransaction implements Serializable {
    private BigDecimal amount;
    private User customer;
    private User merchant;
    private boolean success;
    private String finalMessage;

    public DTUTransaction(){
        this.success = false;
        this.finalMessage = "transaction initialised";
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public User getMerchant() {
        return merchant;
    }

    public void setMerchant(User merchant) {
        this.merchant = merchant;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getFinalMessage() {
        return finalMessage;
    }

    public void setFinalMessage(String finalMessage) {
        this.finalMessage = finalMessage;
    }

    @Override
    public String toString(){
        return "customer '"+this.customer.getCprNumber()+"' paid "+this.amount+" kr to merchant '"+this.merchant.getCprNumber()+"'";
    }
}
