package manager;

import dtu.rs.entities.PaymentData;
import dtu.ws.fastmoney.*;
import entity.local.DTUTransaction;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;

public class SimpleDTUPay {

    private BankService bankService = new BankServiceService().getBankServicePort();

    public void createBankAccount(String firstName, String lastName, String cpr, BigDecimal amount){
        User user = new User();
        user.setCprNumber(cpr);
        user.setFirstName(firstName);
        user.setLastName(lastName);

        try {
            bankService.createAccountWithBalance(user, amount);
        } catch (BankServiceException_Exception e) {
            //e.printStackTrace();
            System.out.println("Account already exists.");
        }
    }

    public void removeBankAccount(String id){
        try {
            bankService.retireAccount(id);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    public BigDecimal getAccountBalance(User user) {
        return getAccountFromActor(user).getBalance();
    }

    public Account getAccountFromActor(User user){
        try {
            return bankService.getAccountByCprNumber(user.getCprNumber());
        } catch (BankServiceException_Exception e) {
            return null;
        }
    }

    public Account getAccountFromCpr(String cpr){
        try {
            return bankService.getAccountByCprNumber(cpr);
        } catch (BankServiceException_Exception e) {
            return null;
        }
    }

    public boolean hasABankAccount(User user){
        return getAccountFromActor(user) != null;
    }

    public Transaction pay(PaymentData paymentData) throws BankServiceException_Exception {
        // BigDecimal amount, String actualCustomer, String actualMerchant
        Transaction transaction = new Transaction();
        transaction.setAmount(paymentData.getAmount());
        transaction.setCreditor(paymentData.getCreditor());
        transaction.setDebtor(paymentData.getDebtor());
        transaction.setDescription(paymentData.getDescription());

        bankService.transferMoneyFromTo(
                paymentData.getDebtor(),
                paymentData.getCreditor(),
                paymentData.getAmount(),
                paymentData.getDescription());

        return transaction;
    }
}
