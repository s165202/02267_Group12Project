package payment;


import dtu.rs.entities.PaymentData;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.Transaction;
import dtu.ws.fastmoney.User;
import entity.local.DTUTransaction;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import manager.SimpleDTUPay;
import rest.RESTClient;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/* Hint:
 * The step classes do not do the HTTP requests themselves.
 * Instead, the tests use the class HelloService, which encapsulates the 
 * HTTP requests. This abstractions help to write easier and more understandable 
 * test classes.
 */
public class PaymentSteps {
	User actualCustomer;
	User actualMerchant;
	User actualActor;
	SimpleDTUPay dtuPay = new SimpleDTUPay();
	boolean successful;
	Transaction lastTransaction;
	PaymentData paymentData = new PaymentData();

	@Given("the customer {string} {string} with CPR {string} has a bank account")
	public void theCustomerWithCPRHasABankAccount(String name, String lastName, String cpr) {
		actualCustomer = new User();
		actualCustomer.setFirstName(name);
		actualCustomer.setLastName(lastName);
		actualCustomer.setCprNumber(cpr);

		actualActor = actualCustomer;
		paymentData.setDebtor(dtuPay.getAccountFromActor(actualActor).getId());
		paymentData.setDescription("Test Test");
		Assertions.assertTrue(dtuPay.hasABankAccount(actualActor));
	}
	@Given("the customer {string} {string} with CPR {string} has not a bank account")
	public void theCustomerWithCPRHasNotABankAccount(String name, String lastName, String cpr) {
		actualCustomer = new User();
		actualCustomer.setFirstName(name);
		actualCustomer.setLastName(lastName);
		actualCustomer.setCprNumber(cpr);

		actualActor = actualCustomer;
		Assertions.assertNull(dtuPay.getAccountFromActor(actualActor));
	}

	@And("the merchant {string} {string} with CPR {string} has a bank account")
	public void theMerchantWithCPRHasABankAccount(String name, String lastName, String cpr) {
		actualMerchant = new User();
		actualMerchant.setFirstName(name);
		actualMerchant.setLastName(lastName);
		actualMerchant.setCprNumber(cpr);

		actualActor = actualMerchant;
		paymentData.setCreditor(dtuPay.getAccountFromActor(actualActor).getId());
		Assertions.assertTrue(dtuPay.hasABankAccount(actualActor));
	}

	@And("the customer is registered with DTUPay")
	public void theCustomerIsRegisteredWithDTUPay() {

	}

	@And("the merchant is registered with DTUPay")
	public void theMerchantIsRegisteredWithDTUPay() {

	}

	@And("the balance of that account is {int}")
	public void theBalanceOfThatAccountIs(int amount) {
		Assertions.assertEquals(BigDecimal.valueOf(amount), dtuPay.getAccountBalance(actualActor));
	}

	@When("the merchant initiates a payment for {int} kr by the customer")
	public void theMerchantInitiatesAPaymentForKrByTheCustomer(int amount) {
		paymentData.setAmount(new BigDecimal(amount));
		try {
			lastTransaction = dtuPay.pay(paymentData);
			successful = true;
		} catch (BankServiceException_Exception e) {
			successful = false;
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Then("the payment is successful")
	public void thePaymentIsSuccessful() {
		Assertions.assertTrue(successful);
	}

	@Then("the payment is not successful")
	public void thePaymentIsNotSuccessful() {
		Assertions.assertFalse(successful);
	}

	@And("the balance of the customer at the bank is {int} kr")
	public void theBalanceOfTheCustomerAtTheBankIsKr(int amount) {
		assertEquals(BigDecimal.valueOf(amount), dtuPay.getAccountBalance(actualCustomer));
	}

	@And("the balance of the merchant at the bank is {int} kr")
	public void theBalanceOfTheMerchantAtTheBankIsKr(int amount) {
		assertEquals(BigDecimal.valueOf(amount), dtuPay.getAccountBalance(actualMerchant));
	}

	@And("an error message is returned saying {string}")
	public void anErrorMessageIsReturnedSaying(String error) {
		//Assertions.assertEquals(error, lastTransaction.getFinalMessage());
	}

	@When("the merchant initiates a payment for {int} kr by the customer from the REST API")
	public void theMerchantInitiatesAPaymentForKrByTheCustomerFromTheRESTAPI(int amount) {

		paymentData.setAmount(new BigDecimal(amount));
		Response response = new RESTClient().proceedPayment(paymentData);
		System.out.println(response);
		Assertions.assertTrue(response.getStatus() == 204);
	}

	@Then("the customer can retrieve the transaction from the REST API")
	public void theCustomerCanRetrieveTheTransactionFromTheRESTAPI() {
		List<Transaction> customerPayments = new RESTClient().getCustomerTransaction(actualCustomer.getCprNumber());
		System.out.println(customerPayments);
	}

	@Before
	public void addAccounts() {
		dtuPay.createBankAccount("Andrew", "Ryan", "061111-0089", new BigDecimal(1000));
		dtuPay.createBankAccount("Frank", "Fontaine", "730521-0041", new BigDecimal(2000));
		System.out.println("Initialisation successful.");
	}

	@After
	public void removeAccounts() {
		String CPR_1 = "061111-0089";
		String CPR_2 = "730521-0041";
		try {
			String id_1 = dtuPay.getAccountFromCpr(CPR_1).getId();
			String id_2 = dtuPay.getAccountFromCpr(CPR_2).getId();
			dtuPay.removeBankAccount(id_1);
			dtuPay.removeBankAccount(id_2);
		} catch (Exception error) {
			System.out.println("Already deleted.");
		}
	}
}
