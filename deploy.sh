#!/bin/bash
set -e

# Move to another directory - useful when we split into microservices
# pushd code-with-quarkus
# Build all images
chmod +x ./build.sh
./build.sh
#popd

# Start all containers and run all images
docker-compose up -d
docker-compose logs -f &

# Install a hook that on err or on normal exit of this script,
# the server is killed, so that we can run the script again
trap 'kill $server_pid' err exit



# Give the Web server a chance to finish start up
sleep 2s

# pushd demo_client
mvn test
# popd

# Shut down the containers and delete
docker-compose down