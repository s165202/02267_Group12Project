package token;

import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.TokenManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assertions.*;
import rest.RESTTokenManagementClient;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class TokenManagerRestTestSteps
{
    RESTTokenManagementClient client = new RESTTokenManagementClient();
    String cid;
    String tid;
    List<Token> tokens;
    Boolean doesFail;
    Token token;

    @Given("REST a customer with id {string} that has {int} tokens")
    public void restACustomerWithIdThatHasTokens(String cid, int numberOfTokens) throws Exception {
        client.clean();
        CreateTokenData tokenData = new CreateTokenData(cid, numberOfTokens);
        List<Token> otherTokens = client.generateTokensForCustomer(tokenData);
        tokens = client.getTokensFromCustomerId(cid);
        this.cid = cid;
        this.doesFail = false;
    }

    @When("the user tries getting a list of tokens for the customer")
    public void theUserTriesGettingAListOfTokensForTheCustomer() {
        try {
            tokens = client.getTokensFromCustomerId(this.cid);
        } catch (Exception e){
            doesFail = true;
        }
    }

    @Then("the user gets the list of {int} tokens")
    public void theUserGetsTheListOfTokens(int numberOfTokens) {
        if(tokens != null){
            Assertions.assertEquals(numberOfTokens,tokens.size());
        } else {
            Assertions.fail("Failed to fetch tokens");
        }
    }

    @When("the user tries requesting a new token for the customer")
    public void theUserTriesRequestingANewTokenForTheCustomer() {
        try {
            CreateTokenData tokenData = new CreateTokenData(cid, 1);
            tokens = client.generateTokensForCustomer(tokenData);
        } catch (Exception e){
            doesFail = true;
        }
    }

    @Then("the request fails")
    public void theRequestFails() {
        Assertions.assertTrue(doesFail);
    }


//    @When("the user tries to get the token from its id")
//    public void theUserTriesToGetTheTokenFromItsId() {
//        try {
//            this.tid = tokens.get(0).getId();
//            token = client.getToken(tid);
//        } catch (Exception e){
//            doesFail = true;
//        }
//
//    }

//    @Then("the user gets the token")
//    public void theUserGetsTheToken() {
//        Assertions.assertEquals(tid,token.getId());
//        Assertions.assertFalse(doesFail);
//    }

    @When("the customer uses one of his tokens")
    public void theCustomerUsesOneOfHisTokens() {
        try {
            List<Token> customerTokens = client.getTokensFromCustomerId(cid);
            this.tid = customerTokens.get(0).getId();
            client.invalidateToken(this.tid);
        } catch (Exception e) {
            doesFail = true;
        }
    }

    @Then("the used token is invalidated")
    public void theUsedTokenIsInvalidated() {
        if (!doesFail){
            try {
                Assertions.assertFalse(client.validateToken(tid));
            } catch (Exception e) {
                Assertions.fail("Token is valid or not found");
            }
        }else{
            Assertions.fail("Token is not found");
        }
    }

    @When("the customer tries to requests {int} new tokens")
    public void theCustomerTriesToRequestNewTokens(int numberOfTokens) {
        CreateTokenData tokenData = new CreateTokenData(cid, numberOfTokens);
        try {
            tokens = client.generateTokensForCustomer(tokenData);
        } catch (Exception e) {
            doesFail = true;
        }
    }

    @Then("the customer gets {int} new tokens")
    public void theCustomerGetsNewTokens(int numberOfTokens) {
        if (tokens != null && !doesFail){
            Assertions.assertEquals(numberOfTokens, tokens.size());
        }else{
            Assertions.fail("Failed to fetch tokens");
        }
    }

	// RESTTokenManagementClient client = new RESTTokenManagementClient();
	// String cid;
	// String tid;
	// Token token;
	// List<Token> currentTokens;
	// boolean hasFailed;

	// @Given("the server is up and running")
	// public void theServerIsUp() throws Exception {
	// 	this.cid = "foo";
	// 	hasFailed = false;
	// 	client.clean();
	// }

	// @And("a customer that has {int} tokens")
	// public void aCustomerThatHasTokens(int numberOfTokens) {
	// 	try {
	// 		currentTokens = client.generateTokensForCustomer(new CreateTokenData(this.cid, numberOfTokens));
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		hasFailed = true;
	// 	}
	// }

	// @When("user tries to get tokens for customer")
	// public void userTriesToGetTokensForCustomer() {
	// 	try {
	// 		currentTokens = client.getTokensFromCustomerId(cid);
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		hasFailed = true;
	// 	}
		
	// }

	// @Then("user gets list of {int} valid tokens")
	// public void userGetsListOfValidTokens(int number) {
	// 	Assertions.assertTrue(currentTokens.stream().allMatch(token -> token.isValid()) && currentTokens.size() == number);
	// 	cleanAllData();
	// }

	// @When("user requests {int} tokens for customer")
	// public void userRequestsTokenForCustomer(int numberOfTokens) {
	// 	try {
	// 		currentTokens = client.generateTokensForCustomer(new CreateTokenData(this.cid, numberOfTokens));
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		hasFailed = true;
	// 	}
	// }

	// @Then("request fails")
	// public void requestFails() {
	// 	Assertions.assertTrue(hasFailed);
	// 	cleanAllData();
	// }

	// @When("the customer uses a token")
	// public void theCustomerUsesAToken() {
	// 	try {
	// 		token = currentTokens.get(0);
	// 		client.invalidateToken(token.getId());
	// 	} catch (Exception e) {
	// 		hasFailed = true;
	// 	}

	// }



	// @Then("the customer has {int} tokens")
	// public void theCustomerHasTokens(int numberOfTokens) {
	// 	try {
	// 		currentTokens = client.getTokensFromCustomerId(cid);
	// 		Assertions.assertEquals(numberOfTokens, currentTokens.size());
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		Assertions.fail("Could not find customer");
	// 	}
	// 	cleanAllData();
	// }


	// @And("an existing valid token")
	// public void anExistingValidToken() {
	// 	try {
	// 		currentTokens = client.getTokensFromCustomerId(cid);
	// 		token = currentTokens.get(0);
	// 		Assertions.assertTrue(client.validateToken(token.getId()));
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		hasFailed = true;
	// 	}
	// }

	// @When("the token is requested by its id")
	// public void theTokenIsRequestedByItsId() {
	// 	tid = token.getId();
	// 	token = client.getToken(tid);
	// }

	// @Then("the REST API returns the token with such id")
	// public void theRESTAPIReturnsTheTokenWithSuchId() {
	// 	Assertions.assertEquals(tid,token.getId());
	// 	cleanAllData();
	// }

	// @When("the customer uses {int} token")
	// public void theCustomerUsesToken(int numberOfTokens) {
	// 	try {
	// 		for (int i = 0; i < numberOfTokens; i++) {
	// 			token = currentTokens.get(i);
	// 			client.invalidateToken(token.getId());
	// 		}
	// 	} catch (Exception e) {
	// 		hasFailed = true;
	// 	}
	// }

	// @Then("the the used token is invalidated")
	// public void theTheUsedTokenIsInvalidated() {
	// 	Assertions.assertTrue(!token.isValid());
	// 	cleanAllData();
	// }

	// @When("the customer requests {int} new tokens")
	// public void theCustomerRequestsTokens(int numberOfTokens) {
	// 	try {
	// 		currentTokens = client.generateTokensForCustomer(new CreateTokenData(cid, numberOfTokens));
	// 	} catch (Exception e) {
	// 		e.printStackTrace();
	// 		hasFailed = true;
	// 	}
		
	// }

	// @Then("the customer receives {int} new tokens")
	// public void theCustomerReceivesNewTokens(int number) {
	// 	Assertions.assertTrue(currentTokens.size() == number);
	// 	cleanAllData();
	// }

	// public void cleanAllData()
	// {
	// 	try {
 	// 		cid = null;
	// 		client.clean();
	// 	} catch (Exception exception) {
	// 		exception.printStackTrace();
	// 		System.out.println("Could not delete data after tests.");
	// 	}
	// }

	// @Given("REST a customer with id {string} that has {int} tokens")
	// public void restACustomerWithIdThatHasTokens(String arg0, int arg1) {

	// }

	// @When("the user tries getting a list of tokens for the customer")
	// public void theUserTriesGettingAListOfTokensForTheCustomer() {
		
	// }

	// @Given("A token with a valid token id")
	// public void aTokenWithAValidTokenId() {
	// }
}