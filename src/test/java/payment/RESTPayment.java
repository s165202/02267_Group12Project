package payment;

import manager.AccountsManager;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class RESTPayment{

    WebTarget baseUrl;
    AccountsManager accountsManager = new AccountsManager();

    public RESTPayment() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
    }

    public AccountsManager getTransactionsManager(){return accountsManager;}

    public String hello() {
        return baseUrl.path("hello-resteasy").request().get(String.class); }

    public boolean testREST(){
        return baseUrl.path("payments").request().get().getStatus() == 200;
    }

    //public DTUTransaction pay(int amount, String cid, String mid) {
    //    DTUTransaction newTransaction = new DTUTransaction();
    //    newTransaction.setCid(cid);
    //    newTransaction.setMid(mid);
    //    newTransaction.setAmount(amount);
    //    DTUTransaction response = baseUrl.path("pay").request()
    //            .accept(MediaType.APPLICATION_JSON)
    //            .post(Entity.entity(newTransaction, MediaType.APPLICATION_JSON), DTUTransaction.class);

    //    transactionsManager.addNewTransaction(response);
    //    return response;
    //}
}
