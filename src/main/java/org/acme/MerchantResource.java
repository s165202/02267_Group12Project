package org.acme;

import dtu.ws.fastmoney.Transaction;
import manager.AccountsManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

@Path("/merchant")
public class MerchantResource {

    static AccountsManager manager = new AccountsManager();

    @GET
    @Path("/payments/{cpr}")
    public List<Transaction> getTransactionsFromCpr(@PathParam("cpr") String cpr){
        return null;
        //return manager.getTransactionsListFromCreditorCpr(cpr);
    }
}
