Feature: Token validation
    Scenario: Customer has a valid unused token
        Given a customer with id "cid" and a valid token
        When user checks whether the customer has the token
        Then the customer has the token

    Scenario: Customer does not have token
        Given a customer with id "cid" and an invalid token
        When user checks whether the customer has the token
        Then the customer does not have the token

    Scenario: Token is valid
        Given a valid token
        When user validates the token
        Then the token is valid

    Scenario: Token is invalid
        Given an invalid token
        When user validates the token
        Then the token is invalid
