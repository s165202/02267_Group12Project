package token;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.TokenManager;
import org.junit.After;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;


public class TokenManagerValidationTestSteps {
	TokenManager tokenManager = new TokenManager();
	String cid;
	String tid;
	boolean customerHasToken;
	boolean tokenIsValid;

	@Given("a customer with id {string} and a valid token")
	public void aCustomerWithIdAndAValidToken(String cid) {
		this.cid = cid;
		try {
			tokenManager.generateTokensForCustomer(cid, 1);
			ArrayList<Token> tokens = tokenManager.getCustomerTokens(cid);
			String tid = tokens.get(0).getId();
			this.tid = tid;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Could not generate token for customer");
		}
	}

	@Given("a customer with id {string} and an invalid token")
	public void aCustomerWithIdAndAnInvalidToken(String cid) {
		this.cid = cid;
		this.tid = "foobarbaz";
	}

	@When("user checks whether the customer has the token")
	public void checksWhetherTheCustomerHasTheToken() {
		customerHasToken = tokenManager.customerHasToken(cid, tid);
	}

	@Then("the customer has the token")
	public void theCustomerHasTheToken() {
		Assertions.assertTrue(customerHasToken);
	}

	@Then("the customer does not have the token")
	public void theCustomerDoesNotHaveTheToken() {
		Assertions.assertFalse(customerHasToken);
	}

	@Given("a valid token")
	public void aValidToken() {
		Token token = tokenManager.generateToken();
		this.tid = token.getId();
	}

	@When("user validates the token")
	public void userValidatesTheToken() {
		tokenIsValid = tokenManager.isValidToken(tid);
	}

	@Then("the token is valid")
	public void theTokenIsValid() {
		Assertions.assertTrue(tokenIsValid);
	}

	@Given("an invalid token")
	public void anInvalidToken() {
		this.tid = "foobarbaz";
	}

	@Then("the token is invalid")
	public void theTokenIsInvalid() {
		Assertions.assertFalse(tokenIsValid);
	}

	@After
    public void clearTokensAndUser()
    {
        tokenManager.removeTokensForUser(cid);
        this.cid = null;
    }
}