package manager;

import dtu.rs.entities.CustomerAccount;
import dtu.rs.entities.DTUPayAccount;
import dtu.rs.entities.MerchantAccount;
import dtu.ws.fastmoney.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AccountsManager {
    private static List<DTUPayAccount> accounts;

    public AccountsManager(){accounts = new ArrayList<>();}

    private String generateID(){
        return UUID.randomUUID().toString();
    }

    public String addNewCustomer(User user) {
        CustomerAccount actualAccount = (CustomerAccount) getAccountFromCPR(user.getCprNumber());

        if(actualAccount != null)
            return actualAccount.getId();

        CustomerAccount newAccount = new CustomerAccount();
        newAccount.setUser(user);
        newAccount.setId(generateID());
        accounts.add(newAccount);
        return newAccount.getId();
    }

    public String addNewMerchant(User user) {
        MerchantAccount actualAccount = (MerchantAccount) getAccountFromCPR(user.getCprNumber());

        if(actualAccount != null)
            return actualAccount.getId();

        MerchantAccount newAccount = new MerchantAccount();
        newAccount.setUser(user);
        newAccount.setId(generateID());
        accounts.add(newAccount);
        return newAccount.getId();
    }

    public DTUPayAccount getAccountFromCPR(String cpr){
        for(int i=0; i<accounts.size(); i++){
            if(accounts.get(i).getUser().getCprNumber().equals(cpr))
                return  accounts.get(i);
        }
        return null;
    }

    public DTUPayAccount getAccount(String id){
        for(int i=0; i<accounts.size(); i++){
            if(accounts.get(i).getId().equals(id))
                return accounts.get(i);
        }
        return null;
    }

    public void modifyAccount(String id, User userInformation){
        getAccount(id).setUser(userInformation);
    }

    public DTUPayAccount popAccount(String id){
        for(int i=0; i<accounts.size(); i++){
            if(accounts.get(i).getId().equals(id))
                return accounts.remove(i);
        }
        return null;
    }

    public boolean isCustomerAccount(String cid){
        return getAccount(cid) instanceof CustomerAccount;
    }

    public boolean isMerchantAccount(String cid){
        return getAccount(cid) instanceof MerchantAccount;
    }

    public List<DTUPayAccount> getAllAccounts(){
        return accounts;
    }
}
