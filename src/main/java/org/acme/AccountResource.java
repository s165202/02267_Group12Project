package org.acme;

import dtu.rs.entities.DTUPayAccount;
import dtu.ws.fastmoney.User;
import manager.SimpleDTUPay;
import manager.AccountsManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/accounts")
public class AccountResource {
    static SimpleDTUPay dtuPay = new SimpleDTUPay();
    AccountsManager accountsManager = new AccountsManager();

    @Path("/customer")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String registerCustomer(User user) {
        if(dtuPay.hasABankAccount(user)) {
            return accountsManager.addNewCustomer(user);
        } else return null;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/customer/{id}")
    public DTUPayAccount getCustomerAccount(@PathParam("id") String id){
        if(accountsManager.isCustomerAccount(id))
            return accountsManager.getAccount(id);
        else
            return null;
    }

    @Path("/merchant")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String registerMerchant(User user) {
        if(dtuPay.hasABankAccount(user)) {
            return accountsManager.addNewMerchant(user);
        } else return null;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/merchant/{id}")
    public DTUPayAccount getMerchantAccount(@PathParam("id") String id){
        if(accountsManager.isMerchantAccount(id))
            return accountsManager.getAccount(id);
        else
            return null;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response changeAccountInformation(@PathParam("id") String id, User user){
        accountsManager.modifyAccount(id, user);
        return Response.ok().build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response retireAccount(@PathParam("id") String id){
        if(accountsManager.popAccount(id) != null){
            return Response.accepted().build();
        }else{
            return Response.serverError().build();
        }
    }

    @GET
    public String getResponse(){
        return "Success";
    }
}