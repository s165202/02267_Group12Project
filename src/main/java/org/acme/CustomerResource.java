package org.acme;

import dtu.rs.entities.DTUPayAccount;
import dtu.ws.fastmoney.Transaction;
import manager.AccountsManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("customer")
public class CustomerResource {

    static AccountsManager accountsManager = new AccountsManager();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/payments/{cpr}")
    public List<Transaction> getTransactionsFromCpr(@PathParam("cpr") String cpr){
        //return manager.getTransactionsListFromDebtorCpr(cpr);
        return new ArrayList<>();
    }

}
