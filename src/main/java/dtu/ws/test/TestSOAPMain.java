package dtu.ws.test;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;

public class TestSOAPMain {

    public static void main(String[] args) {
        BankService bankService = new BankServiceService().getBankServicePort();
        System.out.println(bankService.getAccounts());
    }
}
