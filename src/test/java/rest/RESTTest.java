package rest;

import io.cucumber.java.en.Given;
import org.junit.jupiter.api.Assertions;
import payment.RESTPayment;

public class RESTTest {
    RESTPayment rest = new RESTPayment();

    @Given("the server is up")
    public void theServerIsWorkingFine() {
        Assertions.assertTrue(rest.testREST());
    }
}
