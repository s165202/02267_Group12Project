package dtu.rs.entities;
import dtu.ws.fastmoney.User;

public class DTUPayAccount {
    private User user;
    private String id;

    public DTUPayAccount(){ }

    public User getUser() {
        return user;
    }

    public void setId(String id) {this.id = id;}

    public String getId(){return id;}

    public void setUser(User user) {
        this.user = user;
    }
}
