package rest;

import dtu.rs.entities.DTUPayAccount;
import dtu.rs.entities.PaymentData;
import dtu.ws.fastmoney.Transaction;
import dtu.ws.fastmoney.User;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class RESTClient {
    WebTarget baseUrl;

    public RESTClient() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
    }

    public String hello() {
        return baseUrl.path("hello-resteasy").request().get(String.class);
    }

    public List<Transaction> getCustomerTransaction(String cpr) {
        ArrayList<Transaction> response = baseUrl.path("customer/payments/"+cpr).request()
                .accept(MediaType.APPLICATION_JSON)
                .get(ArrayList.class);

        return response;

    }

    public Response proceedPayment(PaymentData paymentData) {
        Response response = baseUrl.path("payments").request()
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.entity(paymentData, MediaType.APPLICATION_JSON), Response.class);

        return response;
    }
}