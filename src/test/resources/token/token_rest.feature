Feature: REST API testing endpoints

  Scenario: Listing a customers tokens
      Given REST a customer with id "foo" that has 5 tokens
      When the user tries getting a list of tokens for the customer
      Then the user gets the list of 5 tokens

  Scenario: Failing to request tokens when customer has more than 1 token
      Given REST a customer with id "foo" that has 5 tokens
      When the user tries requesting a new token for the customer
      Then the request fails

  Scenario: Using a token, token is invalidated after use
      Given REST a customer with id "foo" that has 5 tokens
      When the customer uses one of his tokens
      Then the used token is invalidated

    Scenario: Generating new tokens for customer
      Given REST a customer with id "foo" that has 1 tokens
      When the customer tries to requests 2 new tokens
      Then the customer gets 2 new tokens